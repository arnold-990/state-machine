﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMesh : MonoBehaviour
{
    private NavMeshAgent NMA;
    private void Start()
    {
        NMA = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Pos;
            if (Physics.Raycast(ray, out Pos))
            {
                NMA.SetDestination(Pos.point);
            }
        }
    }
}