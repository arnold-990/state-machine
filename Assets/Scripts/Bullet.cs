﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "End") 
        {
            gameObject.SetActive(false);
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "End")
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
